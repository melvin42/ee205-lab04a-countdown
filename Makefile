###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04a - Countdown
#
# @file    Makefile
# @version 1.0
#
# @author @Melvin Alhambra <@melvin42@hawaii.edu>
# @brief  Lab 04a - Countdown - EE 205 - Spr 2021
# @date   @todo 10/02/2021
###############################################################################

all: countdown

countdown: countdown.c
	gcc -o countdown countdown.c

clean:
	rm -f *.o countdown
