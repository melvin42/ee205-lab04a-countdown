///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time:  Tue Jan 21 04:26:07 PM HST 2014
//   Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 57 
// @author Melvin Alhambra <melvin42@hawaii.edu>
// @date   10/02/2021
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>


int main() {
   long diffYear,diffDay, diffHour, diffMin, diffSec, diff;
   long secInMin=60;
   long secInHour=secInMin*60;
   long secInDay=secInHour*24;
   long secInYear=secInDay*365;
   struct tm refTime;

   refTime.tm_sec = 0;
   refTime.tm_min = 15;
   refTime.tm_hour = 2;
   refTime.tm_mday = 21;
   refTime.tm_mon = 5;
   refTime.tm_year = 107;
   refTime.tm_wday = 4;

   time_t ref = mktime(&refTime);

   printf("Reference Time:%s", asctime(&refTime));
   
   while(1){
      time_t t = time(NULL);
      diff=difftime(t,ref);
      
      diffYear=diff/secInYear;
      diff-=(diffYear*secInYear);

      diffDay=diff/secInDay;
      diff-=(diffDay*secInDay);

      diffHour=diff/secInHour;
      diff-=(diffHour*secInHour);

      diffMin=diff/secInMin;
      diffSec=diff-(diffMin*secInMin);

      printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %ld\n",diffYear, diffDay, diffHour, diffMin, diffSec);
      sleep(1);
   }
   return 0;

}
